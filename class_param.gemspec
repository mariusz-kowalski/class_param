# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'class_param/version'

Gem::Specification.new do |spec|
  spec.name          = 'class_param'
  spec.version       = ClassParam::VERSION
  spec.authors       = ['Mariusz Kowalski']
  spec.email         = ['mk@devpad.me']

  spec.summary       = 'Adds params to classes'
  spec.description   = 'extend your class with this module to be able to set '\
                       'parameters to your class. Parameter (param) is simple '\
                       'method that returns value defined in class context'
  spec.homepage      = 'https://gitlab.com/mariusz-kowalski/class_param'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`
                       .split("\x0")
                       .reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.12'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'minitest', '~> 5.0'
end
