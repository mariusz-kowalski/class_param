require 'test_helper'

class TestClassParam < Minitest::Test
  class Base # :nodoc:
    extend ClassParam
    param :color
  end

  class Painter < Base # :nodoc:
    color 'red'
    def paint
      color
    end
  end

  def test_class_param
    assert_equal 'red', Painter.new.paint
  end
end
