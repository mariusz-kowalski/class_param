require 'minitest/autorun'
require_relative '../class_param.rb'

class DefaultValueTest < Minitest::Test
  class Base # :nodoc:
    extend ClassParam
    param :color
  end

  class Painter < Base # :nodoc:
    def paint
      color
    end
  end

  def test_class_param
    assert_equal nil, Painter.new.paint
  end
end
