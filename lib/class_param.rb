require 'class_param/version'

# extend your class with this module to be able to set parameters to your class
# parameter (param) is simple method that returns value defined in class context
module ClassParam
  def param(name, default: nil)
    define_singleton_method name do |value|
      define_method name do
        value
      end
    end
    send name, default
  end
end
