ClassParam
==========

extend your class with this module to be able to set parameters to your class
parameter (param) is simple method that returns value defined in class context

Installation
------------

Add this line to your application's Gemfile:

```ruby
gem 'class_param'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install class_param

Usage
-----

```ruby
class Base
  extend ClassParam
  param :color
end

class Painter < Base
  color 'red'
  def paint
    color
  end
end
```

## Development

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab.com at
[issues](https://gitlab.com/mariusz-kowalski/class_param/issues)

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
